Andreu Gimenez Bolinches ([andreu@keio.jp](mailto:andreu@keio.jp))

This report has been written using an interactive Python notebook, which has
been then converted into `pdf`. One can find the source notebook in
the [code
repository](https://gitlab.com/jemaro/keio/space-exploration-engineering/final-report).
The code shown here has been developed prioritizing readability and the
application of the concepts explained during the lectures. The execution
efficiency is then not a priority.

Initially we will import some general libraries and define some utility
functions.


```python
# Python 3.9.7
import numpy as np
from sympy import *
from ipywidgets import *
from typing import Optional, List, Tuple, Union
from IPython.display import display, Math
from functools import lru_cache
from matplotlib import rcParams
from matplotlib_inline.backend_inline import set_matplotlib_formats

rcParams['text.usetex'] = True
rcParams['figure.figsize'] = [8.0, 6.0]
rcParams['savefig.dpi'] = 900
set_matplotlib_formats('pdf', 'retina')

from random import seed, random
seed(0)

import matplotlib.pyplot as plt
from mpl_toolkits.axisartist.axislines import SubplotZero

init_printing()


# Printing utility function
def disp(*expresions: list):
    string = ''
    for expr in expresions:
        if isinstance(expr, str):
            string += expr
        elif isinstance(expr, np.ndarray):
            string += latex(Matrix(expr))
        else:
            string += latex(expr)
    display(Math(string))


# Wrapper for numpy's vector cross product
cross = lambda v1, v2: np.cross(v1, v2, axis=0)
```

# Spin-stabilized spacecraft
![B1](images/B1.jpg)

In order to simulate the dynamics of a spin-stabilized spacecraft, we will use
an object oriented approach. The `Spacecraft` class represents a spacecraft and
it stores its angular velocity and moments of inertia. Usually, we will provide
the moments of inertia to describe an scenario so the default values are not
relevant. But the default values of the initial angular velocities are 
$`1~rad/s`$ for the `z` axis (the spin-stabilized axis) and random values between
$`-0.4`$ and $`0.4~rad/s`$ for the `x` and `y` axes. In this way we will try
different configurations of initial angular velocities across the report
simulations.

We will provide our `Spacecraft` with a `step` method that will update its
state (only the angular velocity in this case). The `step` method will compute
the angular acceleration (using the `dw` method) and multiply it by the
simulation time step in order to update the angular velocity. It is a simple
simulation method which is proven to be stable as long as the time step is
small enough for the dynamic properties of the system. Nevertheless it implies
an accumulated numerical error that we would have to take into account if these
calculations were critical for a project.

Finally, the `Spacecraft` is also equipped with a `simulate` method that will
loop for a certain `duration`, calculating steps with the given `time_step`
and accumulating the state information into a matrix which is returned together
with a time vector. We will use this data later for plotting.


```python
class Spacecraft:
    def __init__(
            self,
            # Arbitrarily given initial angular velocities and moments of
            # inertia
            Ix=2,  # [kg*m^2]
            Iy=2,  # [kg*m^2]
            Iz=1,  # [kg*m^2]
            wx=None,  # [rad/s]
            wy=None,  # [rad/s]
            wz=1,  # [rad/s]
        ):
        if wx is None:
            wx = random() * 0.8 - 0.4  # [rad/s]
        if wy is None:
            wy = random() * 0.8 - 0.4  # [rad/s]
        self.w = np.array([[wx], [wy], [wz]], dtype=np.float64)
        self.Ix = Ix
        self.Iy = Iy
        self.Iz = Iz
        self.I = np.diag([Ix, Iy, Iz])
        self.I_inv = np.linalg.inv(self.I)

    @property
    def wx(self):
        return self.w.item(0)

    @property
    def wy(self):
        return self.w.item(1)

    @property
    def wz(self):
        return self.w.item(2)

    @staticmethod
    def _dw(w, I, I_inv):
        """
        Compute the angular acceleration of the spacecraft
        """
        return -I_inv @ cross(w, I @ w)

    @property
    def dw(self):
        return self._dw(self.w, self.I, self.I_inv)

    def step(self, time_step):
        """
        Simulate one step of the spacecraft dynamics
        """
        self.w += self.dw * time_step
        return self.w

    def simulate(self, duration, time_step):
        """
        Stack together the result of the simulation steps
        """
        t = np.arange(0, duration, time_step)
        step_0 = self.step(0)
        result = np.zeros((step_0.shape[0], len(t)))
        result[:, [0]] = step_0
        for i in range(1, len(t)):
            result[:, [i]] = self.step(time_step)
        return t, result
```

We can check the correctness of our angular acceleration computation by
recreating some calculations of the example 10.4 from the textbook, which
matches with the textbook results.


```python
example_10_4 = Spacecraft(
    wx=0.09086, wy=-0.07709, wz=0.2144, Ix=1000, Iy=2000, Iz=3000
    )
disp(example_10_4.dw)
```


$`\displaystyle \left[\begin{matrix}0.016528096\\0.019480384\\0.00233479913333333\end{matrix}\right]`$


Defined our simulation tool, we need now to implement a way to visualize the
results. We will replicate the graphs from lecture #09-4, slides 21 and 22: We
will draw the time history of the angular velocity `plot_w_time_history` and a
three-dimensional plot of the angular velocities `plot_w_3D`.

We will bundle everything into a `B1` function that creates a `Spacecraft`
given its moments of inertia, simulates it for a given duration and time step,
and plots all the results in the same figure.


```python
def plot_w_time_history(
        t: np.ndarray, w: np.ndarray, ax: Optional[plt.Axes] = None
    ):
    """
    Plot the time history of the angular velocity of the spacecraft
    """
    if ax is None:
        fig, ax = plt.subplots()
    ax.plot(t, w[0, :], '-', t, w[1, :], '--', t, w[2, :], '-.')
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('$`\omega`$ [rad/s]')
    ax.legend(['$`\omega_x`$', '$`\omega_y`$', '$`\omega_z`$'], loc='lower right')
    ax.set_title(f'Time history of angular velocity ({round(t[-1])} seconds)')
    return ax


def plot_w_3D(t: np.ndarray, w: np.ndarray, ax: Optional[plt.Axes] = None):
    """
    Plot the time history of the angular velocity of the spacecraft
    """
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
    ax.plot(w[0, :], w[1, :], w[2, :], lw=0.5)
    ax.set_xlabel('$`\omega_x`$')
    ax.set_ylabel('$`\omega_y`$')
    ax.set_zlabel('$`\omega_z`$')
    ax.set_title('Three-dimensional plot of angular velocities')
    return ax


def B1(title, Ix, Iy, Iz, duration=100, time_step=1e-4):
    """
    Draw the time history of the angular velocities (roll, pitch, and yaw), and
    three-dimensional plot of the angular velocities
    """
    spacecraft = Spacecraft(Ix=Ix, Iy=Iy, Iz=Iz)
    t, w = spacecraft.simulate(duration=duration, time_step=time_step)
    fig = plt.figure()
    fig.subplots_adjust(top=0.85, wspace=0.3, hspace=0.45)
    ax_short_time_history = fig.add_subplot(221)
    plot_w_time_history(
        t[0:int(len(t) / 10)],
        w[:, 0:int(len(t) / 10)],
        ax=ax_short_time_history
        )
    ax_long_time_history = fig.add_subplot(223)
    plot_w_time_history(t, w, ax=ax_long_time_history)
    ax3D = fig.add_subplot(122, projection='3d')
    plot_w_3D(t, w, ax=ax3D)
    if any([Ix == Iy, Ix == Iz, Iy == Iz]):
        title = 'Axially-symmetric spacecraft\n' + title
    else:
        title = 'Axially-asymmetric spacecraft\n' + title
    fig.suptitle(
        title + '\n' + '$`\quad`$'.join([
            f'$`I_{k} = {v}~kg\,m^2`$'
            for k, v in [('x', Ix), ('y', Iy), ('z', Iz)]
            ])
        )
```

We have all the necessary tools to calculate the dynamic behavior of different
spin-stabilized spacecrafts:

## Disk-shape axially-symmetric spacecraft


```python
It = 1
Is = 10
B1('Disk-type', Ix=It, Iy=It, Iz=Is, duration=50, time_step=1e-5)
```


    
![png](README_files/README_9_0.png)
    


As expected, the spinning motion of an axially-symmetrical rigid body without
energy dissipation is always stable. We can see that the `x` and `y`
velocities oscillate between positive and negative values but their phases
differ in a way that when one of them is at its peak, the other is 0. The `z` axis stays
practically constant, which means that the spacecraft will wobble but maintain
the overall attitude.

## Rod-shape axially-symmetric spacecraft


```python
It = 10
Is = 1
B1('Rod-type', Ix=It, Iy=It, Iz=Is, duration=300, time_step=1e-4)
```


    
![png](README_files/README_12_0.png)
    


Again, no energy dissipation is present, so the spinning motion is stable for
the same reasons as for the disk-shape spacecraft.

## Axially-asymmetric spacecraft

Here is where things get interesting. The Dzhanibekov Effect or Tennis Racket
Theorem is explained well in this
[video](https://www.youtube.com/watch?v=1VPfZ_XzisU) that I like. But I will
provide here my own point of view and intuition.

### Spin around the axis of maximum moment of inertia

We have a situation close to the previous disk-shape spacecraft. The spinning
motion is stable, the attitude wobbles around the spin axis but instead of
wobbling in circles as it happened with the axially-symmetric spacecraft
(remember Space/Body cones from lecture #9-2), it wobbles in an ellipsis,
meaning that the nutation angle is not constant. 

This makes the spin velocity $`\omega_z`$ to not be constant anymore. In order to
maintain the same angular momentum, the spin velocity must be higher when the
spacecraft mass is closer to the spin axis. Which happens when the nutation
angle increases. The nutation angle increases together with the absolute
angular velocity along the axes perpendicular to the spinning axis. This
behavior can be observed in the three-dimensional plots of the angular
velocity, when $`\sqrt{\omega_x^2 + \omega_y^2}`$ increases, $`\omega_z`$
increases as well.


```python
B1('Spinning around the axis of maximum moment of inertia (1)', \
    Ix=1, Iy=5, Iz=10)
```


    
![png](README_files/README_15_0.png)
    



```python
B1('Spinning around the axis of maximum moment of inertia (2)', \
    Ix=5, Iy=1, Iz=10)
```


    
![png](README_files/README_16_0.png)
    


### Spin around the axis of minimum moment of inertia

If we don't take into account energy dissipation, the spacecraft will behave
similarly to when spinning around the axis of maximum moment of inertia. The
main difference is that here the spacecraft mass is closer to the spin axis
when the nutation angle is smaller. Therefore the behavior is reversed, when
the absolute angular velocity along the axes perpendicular to the spinning axis
decreases, the spin velocity increases.

In a realistic environment, energy dissipation would make the spacecraft
unstable, which will eventually transition into a spinner around the axis of
maximum inertia as it happened with the [Explorer
One](https://en.wikipedia.org/wiki/Explorer_1#Results) satellite.


```python
B1('Spinning around the axis of minimum moment of inertia (1)', \
    Ix=5, Iy=10, Iz=1)
```


    
![png](README_files/README_18_0.png)
    



```python
B1('Spinning around the axis of minimum moment of inertia (2)', \
    Ix=10, Iy=5, Iz=1)
```


    
![png](README_files/README_19_0.png)
    


### Spin around the axis of intermediate moment of inertia

Spacecraft of this type present a chaotic behavior. This behavior is not stable
as the spacecraft changes completely its attitude periodically. We can see this
behavior in [microgravity
demonstrations](https://www.youtube.com/watch?v=1x5UiwEEvpQ).

The following plots also illustrate this counter-intuitive behavior. The spin
velocity $`\omega_z`$ oscillates between positive and negative values, just
because the body changes its attitude, an observer would see the spacecraft
turning always in the same direction, but doing some switching motion in the
middle. This changes can be more or less sudden depending on the disturbance
angular velocities and the inertial properties of the body.


```python
B1('Spinning around the axis of intermediate moment of inertia (1)', \
    Ix=1, Iy=10, Iz=5)
```


    
![png](README_files/README_21_0.png)
    


The following scenario shows a spin velocity curve that flattens considerably
in its peaks, which is closer to the video linked above. The spacecraft seems
to stabilize but it suddenly changes its attitude again and again.


```python
B1('Spinning around the axis of intermediate moment of inertia (2)', \
    Ix=10, Iy=1, Iz=5)
```


    
![png](README_files/README_23_0.png)
    


# Energy dissipation

![B2_1](images/B2_1.jpg)

![B2_2](images/B2_2.jpg)

If we consider the energy dissipation of the spacecraft, we need to work on the
differential equations used for the simulation. We will use Python's symbolic
module `sympy` for this purpose. Let's start defining the symbolic variables.


```python
m, d, k = symbols('m d k')
wx, wy, wz = symbols('\omega_x \omega_y \omega_z')
dwx, dwy, dwz = symbols('\dot{\omega}_x \dot{\omega}_y \dot{\omega}_z')
Ix, Iy, Iz = symbols('I_x I_y I_z')
z, dz, ddz = symbols('z \dot{z} \ddot{z}')
r = np.array([[0], [1], [0]])
n = np.array([[0], [0], [1]])
w = np.array([[wx], [wy], [wz]])
dw = np.array([[dwx], [dwy], [dwz]])
I = np.diag([Ix, Iy, Iz])

```

Now we will expand the equations defined in lecture #9-4, slide 19,
particularizing them for our damper parameters $`r`$ and $`n`$:


```python
EoM_spacecraft =  I @ dw + cross(w, I @ w) \
    + m*ddz*cross(r, n) + m*dz*cross(w, cross(r, n))
disp(EoM_spacecraft, ' = ', np.zeros((3, 1)))
```


$`\displaystyle \left[\begin{matrix}I_{x} \dot{\omega}_x - I_{y} \omega_{y} \omega_{z} + I_{z} \omega_{y} \omega_{z} + \ddot{z} m\\I_{x} \omega_{x} \omega_{z} + I_{y} \dot{\omega}_y - I_{z} \omega_{x} \omega_{z} + \dot{z} \omega_{z} m\\- I_{x} \omega_{x} \omega_{y} + I_{y} \omega_{x} \omega_{y} + I_{z} \dot{\omega}_z - \dot{z} \omega_{y} m\end{matrix}\right] = \left[\begin{matrix}0\\0\\0\end{matrix}\right]`$



```python
EoM_damper = m * ddz + d * dz + k * z \
    - m * n.T @ cross(r, dw) \
    + m * n.T @ cross(w, cross(w, r + z * n))
EoM_damper = EoM_damper.item()
disp(EoM_damper, '= 0')
```


$`\displaystyle \ddot{z} m + \dot{\omega}_x m + \dot{z} d + k z + m \left(- \omega_{x}^{2} z - \omega_{y} \left(\omega_{y} z - \omega_{z}\right)\right)= 0`$


The equation of motion of the spacecraft can be rewritten taking into account
the equation of motion of the damper as follows:


```python
EoM_spacecraft =  I @ dw + cross(w, I @ w) \
    + (m*ddz - EoM_damper)*cross(r, n) + m*dz*cross(w, cross(r, n))
disp(EoM_spacecraft, ' = ', np.zeros((3, 1)))
```


$`\displaystyle \left[\begin{matrix}I_{x} \dot{\omega}_x - I_{y} \omega_{y} \omega_{z} + I_{z} \omega_{y} \omega_{z} - \dot{\omega}_x m - \dot{z} d - k z - m \left(- \omega_{x}^{2} z - \omega_{y} \left(\omega_{y} z - \omega_{z}\right)\right)\\I_{x} \omega_{x} \omega_{z} + I_{y} \dot{\omega}_y - I_{z} \omega_{x} \omega_{z} + \dot{z} \omega_{z} m\\- I_{x} \omega_{x} \omega_{y} + I_{y} \omega_{x} \omega_{y} + I_{z} \dot{\omega}_z - \dot{z} \omega_{y} m\end{matrix}\right] = \left[\begin{matrix}0\\0\\0\end{matrix}\right]`$


Now we can solve each of the angular acceleration components and the nutation
damper acceleration:


```python
solve = lambda expr, var: simplify(solveset(expr, var).args[0])
for i, dwi in enumerate(dw):
    dwi = dwi.item()
    disp(dwi, '=', solve(EoM_spacecraft.item(i), dwi))
disp(ddz, '=' , solve(EoM_damper, ddz))
```


$`\displaystyle \dot{\omega}_x=\frac{I_{y} \omega_{y} \omega_{z} - I_{z} \omega_{y} \omega_{z} + \dot{z} d - \omega_{x}^{2} m z - \omega_{y}^{2} m z + \omega_{y} \omega_{z} m + k z}{I_{x} - m}`$



$`\displaystyle \dot{\omega}_y=\frac{\omega_{z} \left(- I_{x} \omega_{x} + I_{z} \omega_{x} - \dot{z} m\right)}{I_{y}}`$



$`\displaystyle \dot{\omega}_z=\frac{\omega_{y} \left(I_{x} \omega_{x} - I_{y} \omega_{x} + \dot{z} m\right)}{I_{z}}`$



$`\displaystyle \ddot{z}=\frac{- \dot{\omega}_x m - \dot{z} d - k z + m \left(\omega_{x}^{2} z + \omega_{y} \left(\omega_{y} z - \omega_{z}\right)\right)}{m}`$


Which can be simplified to:

$`\dot{\omega_x} = \frac{\dot{z}d+z(k-m(\omega_x^2+\omega_y^2))+\omega_y\omega_z(I_y-I_z+m)}{I_x-m}`$

$`\dot{\omega_y} = \frac{\omega_z(-\dot{z}m + \omega_x(I_z-I_x))}{I_y}`$

$`\dot{\omega_z} = \frac{\omega_y(\dot{z}m + \omega_x(I_x-I_y))}{I_z}`$

$`\ddot{z} = - \frac{\dot{z}d + z(k-m(\omega_x^2+\omega_y^2)) + m \omega_y \omega_z}{m} - \dot{\omega_x}`$


If we consider the nutation damper position and velocity as spacecraft
parameters that we can update at each step of the simulation (like we did with
the angular velocities in the previous section). Then we can extend the
`Spacecraft` class to be a `SpacecraftWithDamper`. We'll override the `step`
method taking into account the equations described above. Additionally, it will
also return the nutation damper position as the last element of the state
vector. Which will be correctly accumulated in the existing `simulate` method,
there is no need of overriding it.


```python
class SpacecraftWithDamper(Spacecraft):
    # Damper position and orientation
    r = np.array([[0], [1], [0]])
    n = np.array([[0], [0], [1]])
    # Damper parameters
    m = 0.1
    d = 0.02
    k = 0.1
    # Damper initial state
    z, dz = 0, 0

    def step(_, time_step):
        """
        Simulate one step of the spacecraft dynamics
        """
        dwx = (
            _.dz * _.d \
            + _.z * (_.k - _.m * (_.wx**2 + _.wy**2)) \
            + _.wy * _.wz * (_.Iy - _.Iz + _.m)
            ) / (_.Ix - _.m)
        dwy = _.wz / _.Iy * (
            -_.dz * _.m + _.wx * (_.Iz - _.Ix)
            )
        dwz = _.wy / _.Iz * (
            _.dz * _.m + _.wx * (_.Ix - _.Iy)
            )
        # Custom code formatting
        # yapf: disable
        ddz = - dwx - (
            _.dz * _.d
            + _.z * (_.k - _.m * (_.wx**2 + _.wy**2))
            + _.wy * _.wz * _.m
            ) / _.m
        # yapf: enable
        _.w += np.array([[dwx], [dwy], [dwz]], dtype=np.float64) * time_step
        _.dz += ddz * time_step
        _.z += _.dz * time_step
        return np.append(_.w, [[_.z]], axis=0)
```

Then we need to add a plotting function for the nutation damper position
`plot_z_time_history` and we can bundle everything in a function `B2`. Which,
similarly to `B1`, creates a `SpacecraftWithDamper`, simulates it, and plots
the results.


```python
def plot_z_time_history(
        t: np.ndarray, w: np.ndarray, ax: Optional[plt.Axes] = None
    ):
    """
    Plot the time history of the nutation-damper displacement
    """
    if ax is None:
        fig, ax = plt.subplots()
    ax.plot(t, w[3, :], '-', lw=0.5)
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('z [m]')
    ax.set_title('Time history of nutation-damper displacement')
    return ax


def B2(title, Ix, Iy, Iz, duration=1000, time_step=1e-4):
    """
    Draw the time history of the angular velocities (roll, pitch, and yaw), the
    nutation damper and three-dimensional plot of the angular velocities
    """
    spacecraft = SpacecraftWithDamper(Ix=Ix, Iy=Iy, Iz=Iz, wx=0.4, wy=0.4)
    t, w = spacecraft.simulate(duration=duration, time_step=time_step)
    fig = plt.figure()
    fig.subplots_adjust(top=0.85, wspace=0.3, hspace=0.45)
    ax_short_time_history = fig.add_subplot(221)
    plot_w_time_history(
        t[0:int(len(t) / 10)],
        w[:, 0:int(len(t) / 10)],
        ax=ax_short_time_history
        )
    ax_long_time_history = fig.add_subplot(222)
    plot_w_time_history(t, w, ax=ax_long_time_history)
    ax_z = fig.add_subplot(223)
    plot_z_time_history(t, w, ax=ax_z)
    ax3D = fig.add_subplot(224, projection='3d')
    plot_w_3D(t, w, ax=ax3D)
    if any([Ix == Iy, Ix == Iz, Iy == Iz]):
        title = 'Axially-symmetric spacecraft\n' + title
    else:
        title = 'Axially-asymmetric spacecraft\n' + title
    fig.suptitle(
        title + '\n' + '$`\quad`$'.join([
            f'$`I_{k} = {v}~kg\,m^2`$'
            for k, v in [('x', Ix), ('y', Iy), ('z', Iz)]
            ])
        )
```

Our simulations give results that match the ones from lecture
#9-4, slides 21 and 22. The nutation damper stabilizes the system if the
axis-symmetrically spacecraft is turning around its axis of maximum moment of inertia.


```python
It = 1
Is = 10
B2('Disk-type', Ix=It, Iy=It, Iz=Is)
```


    
![png](README_files/README_40_0.png)
    


But the energy dissipation makes the system unstable if the axis-symmetrically
spin-stabilized spacecraft is turning around its axis of minimum moment of
inertia.


```python
It = 10
Is = 1
B2('Rod-type', Ix=It, Iy=It, Iz=Is)
```


    
![png](README_files/README_42_0.png)
    

