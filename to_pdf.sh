#!/bin/bash
cd "$(dirname "$0")"
FILENAME="Final Report.ipynb"

# Convert the notebook to pdf
jupyter nbconvert --to=pdf "$FILENAME" \
    --output="SEE - Andreu Gimenez"

# Create a README.md from the notebook with Gitlab Math Markdown
jupyter nbconvert --to=markdown "$FILENAME" \
    --output="README"
cat README.md \
    | tr '\n' '\f' \
    | sed -E 's/\$\$(\s*)([^$]+)(\s*)\$\$/\`\`\`math\n\2\n\`\`\`/gm' \
    | sed -E 's/\$([^$\n\f\r]+)\$/\$\`\1\`\$/g' \
    | tr '\f' '\n' > README.md